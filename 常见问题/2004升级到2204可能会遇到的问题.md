# <center>2004升级到2204可能会遇到的问题</center>
#### <center>作者：优麒麟</center>
#### <center>2022-04-22 23:49:17</center>

在2204发布初期此文档和腾讯文档的[2004升级2204可能出现的问题](https://docs.qq.com/doc/https://docs.qq.com/doc/DWEFMQU1HVkRaa2NX)进行双向同步，部分内容为方便展示会临时放在腾讯文档并尽可能快的同步至此

此处升级特指使用公众号文章里的命令升级，重装请无视，并不是所有问题都会出现，每个人的情况不同
建议：关闭锁屏，关闭休眠，千万不要自己中断升级过程或者强制关机，建议在自己空闲的时候升级

1. 升级过程中卡到了火狐浏览器？
- 答：并不是卡住不动了，是在下载火狐，稍微等等看
2. 搜狗输入法不能使用了或者按键反应很慢？
- 答：先切换到终端，输入fcitx -r，然后切换到搜狗如果提示有libidn.so这个库就手动安装libidn11这个库，先切换到别的输入法，然后

    ```
    sudo apt install libidn11 
    ```

    也可能缺少liblber-2.4.so.2，也需要手动安装libldap-2.4-2

    ```
    sudo apt install libldap-2.4-2
    ```

    之后重新输入fcitx  -r，关闭所有终端即可

3. 腾讯文档原生版本打不开了？
- 答：需要修改启动的快捷方式，主要原因是腾讯没有适配新版本
    终端输入

    ```
    sudo pluma /usr/share/applications/com.qq.docs.tdappdesktop.desktop
    ```

    然后找到exec那行在最后加上--no-sandbox参数，变成类似这样的

    ```
    /opt/apps/com.qq.docs.tdappdesktop/files/bin/tdappdesktop %U --no-snadbox
    ```

    刚刚测试发现，加上参数了只是能显示登录界面，我这里没办法成功登录

4. 软件商店有问题无法打开？
- 答：首先看一下商店是否是用了旧版的商店可以使用下方的命令查看

    ```
    dpkg -l|grep ubuntu-kylin-software-center
    ```

    如果是类似下方的内容出现则是因为商店的版本错误

    ```
    ii ubuntu-kylin-software-center
    ```

    通过执行下方命令重新安装新版商店即可

    ```
    sudo apt install kylin-software-center
    ```

5. 火狐浏览器打开下载文件夹，提示无法获取系统图标
-   答：通过执行以下命令，然后重启：

    ```
    sudo sed -i '$a QT_QPA_PLATFORMTHEME=ukui' /etc/environment
    ```
