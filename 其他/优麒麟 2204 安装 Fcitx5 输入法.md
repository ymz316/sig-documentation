# <center>优麒麟 22.04 安装 Fcitx5 输入法</center>
#### <center>作者：ymz316</center>
#### <center>2022-06-09</center>
<br>

Fcitx5 是继 Fcitx 之后的新一代输入法框架，优麒麟2204 默认安装的是 Fcitx，而 Fcitx 和 Fcitx5 是相互冲突的，因此安装Fcitx5之前需要先卸载 Fcitx。关于 Fcitx5 的学习文档，推荐 Archlinux 关于 Fcitx5 的 [wiki 文档](https://wiki.archlinux.org/title/Fcitx5_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))

## 一、卸载 Fcitx 输入法

### **1.首先看看大概装了哪些 Fcitx 输入法组件**

```
$ apt search fcitx | grep 安装
fcitx/jammy,jammy,now 1:4.2.9.8-5 all [已安装]
fcitx-baidupinyin/jammy-partner,now 1.0.1.0 amd64 [已安装]
fcitx-bin/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-config-common/jammy,jammy,now 0.4.10-3 all [已安装]
fcitx-config-gtk/jammy,now 0.4.10-3 amd64 [已安装]
fcitx-data/jammy,jammy,now 1:4.2.9.8-5 all [已安装]
fcitx-frontend-all/jammy,jammy,now 1:4.2.9.8-5 all [已安装]
fcitx-frontend-gtk2/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-frontend-gtk3/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-frontend-qt5/jammy,now 1.2.7-1.2build1 amd64 [已安装]
fcitx-googlepinyin/jammy,now 0.1.6-5 amd64 [已安装]
fcitx-libs/jammy,jammy,now 1:4.2.9.8-5 all [已安装，自动]
fcitx-module-cloudpinyin/jammy,now 0.3.7-1 amd64 [已安装]
fcitx-module-dbus/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-module-kimpanel/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-module-lua/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-module-quickphrase-editor5/jammy,now 1.2.7-1.2build1 amd64 [已安装]
fcitx-module-x11/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-modules/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-pinyin/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-table/jammy,now 1:4.2.9.8-5 amd64 [已安装]
fcitx-ui-classic/jammy,now 1:4.2.9.8-5 amd64 [已安装]
libfcitx-config4/jammy,now 1:4.2.9.8-5 amd64 [已安装]
libfcitx-core0/jammy,now 1:4.2.9.8-5 amd64 [已安装]
libfcitx-gclient1/jammy,now 1:4.2.9.8-5 amd64 [已安装]
libfcitx-qt5-1/jammy,now 1.2.7-1.2build1 amd64 [已安装]
libfcitx-qt5-data/jammy,jammy,now 1.2.7-1.2build1 all [已安装]
libfcitx-utils0/jammy,now 1:4.2.9.8-5 amd64 [已安装]
```

### **2.卸载上面所有的包**

```
$ sudo apt remove fcitx-data fcitx-pinyin fcitx-baidupinyin fcitx-googlepinyin fcitx-bin fcitx-libs fcitx-config-common fcitx-frontend-* fcitx-modules libfcitx-*
```

也可以分步删除：

```
$ sudo apt remove fcitx-data             # 会删除：fcitx fcitx-data fcitx-table sogouimebs
$ sudo apt remove fcitx-pinyin fcitx-baidupinyin fcitx-googlepinyin
$ sudo apt remove fcitx-bin
$ sudo apt remove fcitx-libs
$ sudo apt remove fcitx-config-common    # 会删除：fcitx-config-common fcitx-config-gtk
$ sudo apt remove fcitx-frontend-*       # 会删除：fcitx-frontend-all fcitx-frontend-gtk2 fcitx-frontend-gtk3 fcitx-frontend-qt5
$ sudo apt remove fcitx-modules          # 会删除： fcitx-module-cloudpinyin fcitx-module-dbus fcitx-module-kimpanel fcitx-module-lua fcitx-module-x11 fcitx-modules fcitx-ui-classic
$ sudo apt remove fcitx-module-quickphrase-editor5
$ sudo apt remove libfcitx-*             # 会删除： libfcitx-config4 libfcitx-core0 libfcitx-gclient1 libfcitx-qt5-1 libfcitx-qt5-data libfcitx-utils0
```

## 二、安装 Fcitx5

Fcitx5 输入法框架的软件包主要包括：输入法主体 fcitx5，输入法引擎，支撑图形界面的组件，输入法模块，以及输入法配置工具等。

### **1.安装输入法主体**

```
$ sudo apt install fcitx5
```

下面是安装过程中的有关提示信息（发出来的目的是为了更好地理解 Fcitx5 的框架结构）：

> 建议安装：  fcitx5-chinese-addons fcitx5-frontend-gtk2 kde-config-fcitx5  
> 推荐安装：  fcitx5-config-qt fcitx5-frontend-gtk3 fcitx5-frontend-gtk4 fcitx5-frontend-qt5 fcitx5-module-emoji fcitx5-module-quickphrase fcitx5-module-wayland fcitx5-module-xorg  
> 下列【新】软件包将被安装：  fcitx5 fcitx5-data fcitx5-modules libfcitx5config6 libfcitx5core7 libfcitx5utils2 libxcb-ewmh2

### **2.安装中文输入法引擎**

`fcitx5-chinese-addons` 是 Fcitx5 的一个中文输入法引擎，它包含了大量中文输入方式：拼音、双拼、五笔拼音、自然码、仓颉、冰蟾全息、二笔等。

```
$ sudo apt install fcitx5-chinese-addons
```

下面是安装过程中的有关提示信息：

> 推荐安装：  fcitx5-module-chttrans fcitx5-module-cloudpinyin fcitx5-module-fullwidth fcitx5-module-quickphrase  
> 下列【新】软件包将被安装：  fcitx5-chinese-addons fcitx5-chinese-addons-bin fcitx5-chinese-addons-data fcitx5-module-pinyinhelper fcitx5-module-punctuation fcitx5-pinyin fcitx5-table libfcitx5-qt-data libfcitx5-qt1 libime-data libime-data-language-model libimecore0 libimepinyin0 libimetable0

### **3.安装输入法对图形界面的支持组件**

```
$ sudo apt install fcitx5-frontend-*
```

下面是安装过程中的有关提示信息：

> 建议安装：  libgtk-4-media-gstreamer | libgtk-4-media-ffmpeg  
> 推荐安装：  libgtk-4-bin  
> 下列【新】软件包将被安装：  fcitx5-frontend-gtk2 fcitx5-frontend-gtk3 fcitx5-frontend-gtk4 fcitx5-frontend-qt5 libcairo-script-interpreter2 libfcitx5gclient2 libgtk-4-1 libgtk-4-common

### **4.安装配置工具**

```
$ sudo apt install fcitx5-config-qt kde-config-fcitx5
```

下面是安装过程中的有关提示信息：

> 推荐安装：  libime-bin  
> 下列【新】软件包将被安装： fcitx5-config-qt kde-cli-tools kde-cli-tools-data kde-config-fcitx5 libkf5pty-data libkf5pty5 libkf5su-bin libkf5su-data libkf5su5 libkworkspace5-5 qml-module-org-kde-kcm

### **5.安装推荐的输入法模块**

```
$ sudo apt install fcitx5-module-chttrans fcitx5-module-cloudpinyin fcitx5-module-fullwidth fcitx5-module-quickphrase fcitx5-module-emoji fcitx5-module-quickphrase fcitx5-module-wayland fcitx5-module-xorg
```

下面是安装过程中的有关提示信息：

> 将会同时安装下列软件： libmarisa0 libopencc-data libopencc1.1 libxcb-imdkit1 unicode-cldr-core  
> 下列【新】软件包将被安装：fcitx5-module-chttrans fcitx5-module-cloudpinyin fcitx5-module-emoji fcitx5-module-fullwidth fcitx5-module-quickphrase fcitx5-module-wayland fcitx5-module-xorg libmarisa0 libopencc-data libopencc1.1 libxcb-imdkit1 unicode-cldr-core

如无意外，重启后会自动启动 Fcitx5 输入法了。

## 三、配置Fcitx5 输入法

### **1.安装主题**

安装并选择主题包，可以改善输入法托盘的外观，增加文字输入的舒适感。这里我下载和安装一下 fcitx5-nord 主题（主题包来源：Archlinux 关于 Fcitx5 的 wiki 文档中的主题介绍），安装命令如下：

```
$ git clone https://github.com/tonyfettes/fcitx5-nord.git     # 克隆 fcitx5-nord 主题包到本地
$ cd fcitx5-nord/                                             # 切换到 fcitx5-nord 主题包目录
$ ls                                                          # 查看目录内文件，发现有2个主题
LICENSE  Nord-Dark  Nord-Light  README.md
$ cp -r Nord-Dark/ Nord-Light/ ~/.local/share/fcitx5/themes/  # 复制2个主题到 Fcitx5 的主题目录
$ fcitx5-configtool                                           # 配置主题及其他参数
```

配置主题界面如下：

![主题选择](../assets/Fcitx5/Fcitx5主题选择.png)

### **2.安装词库**

安装离线词库后可以快速从词库中进行联想匹配，从而极大地提高输入速度及准确率。

Fcitx5的离线词库一般放在 `~/.local/share/fcitx5/pinyin/dictionaries` 目录，这些离线词库通常命名为 `*.dict`。安装方法也很简单，就是下载离线词库，将其复制到 `~/.local/share/fcitx5/pinyin/dictionaries` 即可（注意：如果找不到 dictionaries 目录则可以自己建立）。

**常用的离线词库：**  
- 萌娘百科词库 https://github.com/outloudvi/mw2fcitx  
- 维基百科词库 https://github.com/felixonmars/fcitx5-pinyin-zhwiki  

从上面2个仓库的 Releases 找到并下载词库，然后复制到词库目录，再查看词库目录应该是类似这样的：

```
$ ls ~/.local/share/fcitx5/pinyin/dictionaries
moegirl.dict  zhwiki-20220529.dict
```

重启或重启输入法生效，这时就能体验快速输入某些生僻词汇了。